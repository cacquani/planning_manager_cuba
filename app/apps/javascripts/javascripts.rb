# For now, it serves plain js.
# Maybe extend for coffee script later?
Javascripts = Cuba.new do
  self.settings[:views] = 'app/apps/javascripts/views'

  on get do
    on '([^\/]+).js' do |script|
      file = File.join(self.settings[:views], "#{script}.js")
      if(File.exists?(file))
        res.headers['Content-Type'] = "text/javascript; charset=utf-8"
        res.write File.read(file)
      end
    end
  end
end

