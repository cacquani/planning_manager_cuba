# https://gist.github.com/jamiehodge/1327195
# http://pothibo.com/2013/07/authentication-with-warden-devise-less/
# http://sequel.jeremyevans.net/documentation.html

Sequel.extension :migration
Sequel.migration do 
  change do
    create_table(:users) do
      primary_key :id
      String   :email, null: false, unique: true
      String   :encrypted_password, null: false
      String   :confirmation_token, null: true, unique: true
      DateTime :confirmed_at
      DateTime :confirmation_sent_at

      DateTime :created_at, default: 'NOW()'
      DateTime :updated_at, default: 'NOW()'
    end
    add_index :users, :email, unique: true
    add_index :users, :confirmation_token, unique: true
  end
end
