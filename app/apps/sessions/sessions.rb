Sessions = Cuba.new do
  self.settings[:render][:template_engine]  = 'slim'
  self.settings[:render][:views]            = 'app/views'

  on get do
    on 'new' do
      redirect '/' unless env['warden'].unauthenticated?
      render( "sessions/new" )
    end
  end

  on post do
    on '' do
      env['warden'].authenticate!
      redirect '/'
    end
  end

  on delete do
    on '' do
      env['warden'].raw_session.inspect
      env['warden'].logout
      redirect '/'
    end
  end
end
