class User < Sequel::Model
  def self.authenticate(name, password)
    user = self.first(name: name)
    user if user && user.password == password
  end
end
