# Serves plain css stylesheets
# or interprete sacs to css
require 'sass'

Stylesheets = Cuba.new do
  self.settings[:render][:template_engine]  = 'scss'
  self.settings[:render][:views]            = 'app/apps/stylesheets/views'

  on get do
    on '([^\/]+).css' do |style|
      file = File.join(self.settings[:render][:views], "#{style}.css")
      if(File.exists?(file))
        res.headers['Content-Type'] = "text/css; charset=utf-8"
        res.write File.read(file)
      else
        res.headers['Content-Type'] = "text/css; charset=utf-8"
        res.write partial("#{style}")
      end
    end
  end
end
