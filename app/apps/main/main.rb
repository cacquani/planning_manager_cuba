Main = Cuba.new do
  self.settings[:render][:template_engine]  = 'slim'
  self.settings[:render][:views]            = 'app/views'

  on get do
    env['warden'].authenticate!(:password)
    render( 'home/index' )
  end
end
