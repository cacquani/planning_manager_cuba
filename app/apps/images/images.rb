# Serves images
Images = Cuba.new do
  self.settings[:render][:views] = 'app/apps/images/views'

  on get do
    on '([^\/]+)' do |image|
      file = File.join(self.settings[:render][:views], "#{image}")
      if(File.exists?(file))
        res.headers['Content-Type'] = mime(file)
        res.write File.read(file)
      end
    end
  end
end
