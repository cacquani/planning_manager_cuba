require 'cuba'

# require all initialisers
require './initialisers/loader.rb'

# require all the apps
Dir['./app/apps/**/*.rb'].each do |app|
  require app
end

Cuba.define do
  # serve stylesheets from a separate app
  # both plain css and interpreted scss
  on 'stylesheets' do
    run Stylesheets
  end

  # serve javascripts from a separate app
  # just plain js for now, maybe coffeescript later
  on 'javascripts' do
    run Javascripts
  end

  # serve images from a separate app
  on 'images' do
    run Images
  end

  # Sessions logic
  on 'session' do
    run Sessions
  end

  # landing page
  on root do
    run Main
  end
end
