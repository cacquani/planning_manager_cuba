require_relative '../acceptance_helper'

scope 'should interprete images as images' do
  setup do
    get '/images/InfinityKey.gif'
  end

  test 'should be successful' do
    assert_equal 200, last_response.status
  end

  test 'should be an image/gif' do
    assert /^image\/gif/.match(last_response.headers["Content-Type"])
  end
end
