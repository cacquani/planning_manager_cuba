require_relative '../acceptance_helper'

scope 'should serve plain js' do
  setup do
    get '/javascripts/javascript.js'
  end

  test 'should be successful' do
    assert_equal 200, last_response.status
  end

  test 'should be a text/javascript' do
    assert /^text\/javascript/.match(last_response.headers["Content-Type"])
  end
end
