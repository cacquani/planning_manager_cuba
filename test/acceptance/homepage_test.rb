require_relative '../acceptance_helper'

scope 'Homepage' do
  setup do
    get '/'
  end

  test 'should respond' do
    assert_equal 200, last_response.status
  end

  test 'should have the right title' do
    assert /<h1 class="logo">Planning Manager<\/h1>/.match(last_response.body)
  end

  test 'should internationalise text' do
    assert /#{I18n.t('hello')}/.match(last_response.body)
  end
end
