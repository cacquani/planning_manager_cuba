require 'rack/protection'

Cuba.use Rack::Session::Cookie, :secret => 'CTUfGzZjNt1l+##PYWHM7AW_JUdm-21w0wFxMKH33B!FhK'
Cuba.use Rack::Protection
Cuba.use Rack::Protection::RemoteReferrer #(not included by use Rack::Protection)
Cuba.use Rack::Protection::AuthenticityToken #(not included by use Rack::Protection)
Cuba.use Rack::Protection::FormToken #(not included by use Rack::Protection)
Cuba.use Rack::Protection::EscapedParams #(not included by use Rack::Protection)
Cuba.use Rack::Protection::XSSHeader #(Internet Explorer only)
#Cuba.use Rack::Protection::JsonCsrf
#Cuba.use Rack::Protection::RemoteToken
#Cuba.use Rack::Protection::HttpOrigin
#Cuba.use Rack::Protection::FrameOptions
#Cuba.use Rack::Protection::PathTraversal
#Cuba.use Rack::Protection::SessionHijacking
#Cuba.use Rack::Protection::IPSpoofing
