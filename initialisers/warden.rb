require 'warden'

# method override for put, delete
Cuba.use Rack::MethodOverride

Warden::Strategies.add(:password) do
  def authenticate!
    if (params['user'] &&
        params['user']['username'] == 'test' && 
        params['user']['password'] == 'test')
      success!(params['user']['username'])
      redirect!('/')
    else
      fail!('Could not log in. Username or password incorrect.')
      redirect!('/session/new')
    end
  end
end

Warden::Manager.before_failure do 
  env['REQUEST_METHOD'] = 'POST'
end

Cuba.use Warden::Manager do |manager|
  manager.serialize_into_session(&:user)
  manager.serialize_from_session { |user| user }
  manager.default_strategies :password
  manager.failure_app = Sessions
end
