require 'i18n'

I18n.load_path += Dir['./initialisers/locales/*.yml']
I18n.available_locales = ["en"]
I18n.default_locale = "en"
