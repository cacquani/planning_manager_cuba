require 'cuba/render'
require 'slim'
require 'sass'

# require cuba/render here globally 
# to avoid duplicating it in the apps that use it
Cuba.plugin Cuba::Render
