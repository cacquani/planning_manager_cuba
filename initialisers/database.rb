require 'sequel'

ENV['SQLITE_CONNECT'] = 'sqlite://database/demo.db'
SQLITE = Sequel.sqlite(ENV['SQLITE_CONNECT'])

# require all the apps
Dir['./app/apps/**/*.rb'].each do |app|
  require app
end
