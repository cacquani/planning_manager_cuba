require 'filemagic'

def mime(file)
  type = 'text/html'
  FileMagic.open(:mime) { |fm| type = fm.file(file) }
  type
end
